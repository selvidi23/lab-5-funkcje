# README #

Proszę napisać program, umożliwiający narysowanie następujących figur (każdy punkt to jedna funkcja). Dodatkowo menu powinno być wyświetlane przez oddzielną funkcję.  
Linia pozioma $$$$$ (parametry: długość i znak)  
Pojedynczy szlaczek $$$$$*****$$$$$***** (korzysta z 1.; parametry: długość $/* (5) i ilość powtórzeń (2))  
Prostokąt (korzysta z 1; parametry: długości boków (5, 2) i znak ($))  
$$$$$  
$$$$$  
Kwadrat (korzysta z 3; parametry: długość boku (5) i znak ($))  
$$$$$  
$$$$$  
$$$$$  
$$$$$  
$$$$$  
Pojedynczy szlaczek premium (korzysta z 1.; parametry: długość $/* (5), ilość powtórzeń (2) i znaki ($, *))  
 $$$$$*****$$$$$*****  
Szeroki szlaczek (korzysta z 5; parametry: długość $/* (5), ilość powtórzeń (2), grubość i znaki ($, *))  
$$$$$*****$$$$$*****  
$$$$$*****$$$$$*****  
$$$$$*****$$$$$*****  
Szachownica (korzysta z 6; parametry: bok pola (5), bok szachownicy (4), znaki ($, *))  
$$$$$*****$$$$$*****  
$$$$$*****$$$$$*****  
$$$$$*****$$$$$*****  
*****$$$$$*****$$$$$  
*****$$$$$*****$$$$$  
*****$$$$$*****$$$$$  