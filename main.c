#include <stdio.h>

// Proszę napisać program, umożliwiający narysowanie następujących figur (każdy punkt to jedna funkcja). Dodatkowo menu powinno być wyświetlane przez oddzielną funkcję.
// Linia pozioma $$$$$ (parametry: długość i znak)
// Pojedynczy szlaczek $$$$$*****$$$$$***** (korzysta z 1.; parametry: długość $/* (5) i ilość powtórzeń (2))
// Prostokąt (korzysta z 1; parametry: długości boków (5, 2) i znak ($))
// $$$$$
// $$$$$
// Kwadrat (korzysta z 3; parametry: długość boku (5) i znak ($))
// $$$$$
// $$$$$
// $$$$$
// $$$$$
// $$$$$
// Pojedynczy szlaczek premium (korzysta z 1.; parametry: długość $/* (5), ilość powtórzeń (2) i znaki ($, *))
//  $$$$$*****$$$$$*****
// Szeroki szlaczek (korzysta z 5; parametry: długość $/* (5), ilość powtórzeń (2), grubość i znaki ($, *))
// $$$$$*****$$$$$*****
// $$$$$*****$$$$$*****
// $$$$$*****$$$$$*****
// Szachownica (korzysta z 6; parametry: bok pola (5), bok szachownicy (4), znaki ($, *))
// $$$$$*****$$$$$*****
// $$$$$*****$$$$$*****
// $$$$$*****$$$$$*****
// *****$$$$$*****$$$$$
// *****$$$$$*****$$$$$
// *****$$$$$*****$$$$$

void h_line(int length, char character){
    for(int i=0;i<length;i++){
        printf("%c",character);
    }
}

void single_trail(int length, int number_of_repetitions){
    for(int i =0;i<number_of_repetitions;i++){
        h_line(length,'$');
        h_line(length,'*');
    }
}

void print_rectangle(int a,int b, char character){
    for(int i = 0; i<a; i++){
        h_line(b,character);
        printf("\n");
    }

}


void print_square(int a,char character){

    print_rectangle(a,a,character);
}

void single_tail_plus(int a, int number_of_repetitions,char character_A, char character_B){
    for(int i =0; i<number_of_repetitions;i++){
        h_line(a,character_A);
        h_line(a,character_B);
    }
}

void wide_trail(int length, int number_of_repetitions, int thickness, char character_A, char character_B){
    for(int i=0;i<thickness;i++){
        single_tail_plus(length,number_of_repetitions,character_A,character_B);
        printf("\n");
    }
    
}

void print_chess(int length_trail, int length, char character_A, char character_B){
    for(int i =0;i<length/2;i++){
        wide_trail(length_trail,length/2,length_trail,character_A,character_B);
        wide_trail(length_trail,length/2,length_trail,character_B,character_A);
    
    }
    
}

void menu(){
    int select;
    int a,b,c;
    char character_A,character_B;
    do{

    
        printf("\n");
        printf("Wybierz dzialanie\n\n");

        printf("1 - Pozioma linia \n");
        printf("2 - Pojedynczy szlaczek \n");
        printf("3 - Prostokat\n");
        printf("4 - Kwadrat\n");
        printf("5 - Pojedynczy szlaczek premium  \n");
        printf("6 - Szeroki szlaczek\n");
        printf("7 - Szachownica \n");

        printf("0 - Koniec\n\n");

        scanf("%d",&select);

        printf("\n");
   
   switch (select)
    {
    case 1:
        printf("Podaj dlugosc\n");
        scanf("%d",&a);
        printf("Podaj znak \n");
        scanf(" %c",&character_A);

        h_line(a,character_A);
        
        break;
    case 2:
        printf("Podaj dlugosc\n");
        scanf("%d",&a);
        printf("Podaj liczbe powtorzen \n");
        scanf("%d",&b);
        single_trail(a,b);
        break;
    case 3:
        printf("Podaj wysokosc boku a\n");
        scanf("%d",&a);
        printf("Podaj dlugosc bok b\n");
        scanf("%d",&b);
        printf("Podaj znak\n");
        scanf(" %c",&character_A);
        print_rectangle(a,b,character_A);

        break;
    case 4:
        printf("Podaj dlugosc bokow \n");
        scanf("%d",&a);
        printf("Podaj znak");
        scanf(" %c",&character_A);
        print_square(a,character_A);
        break;
    case 5:
        printf("Podaj dlugosc a \n");
        scanf("%d",&a);
        printf("Podaj liczba powtorzen  \n");
        scanf("%d",&b);
        printf("Podaj pierwszy znak \n");
        scanf(" %c",&character_A);
        printf("Podaj drugi znak \n");
        scanf(" %c",&character_B);
        single_tail_plus(a,b,character_A,character_B);
        
        break;
    case 6:
        printf("Podaj dlugosc a \n");
        scanf("%d",&a);
        printf("Podaj liczba powtorzen  \n");
        scanf("%d",&b);
        printf("Podaj wyskosc c \n");
        scanf("%d",&c);
        printf("Podaj pierwszy znak \n");
        scanf(" %c",&character_A);
        printf("Podaj drugi znak \n");
        scanf(" %c",&character_B);
        wide_trail(a,b,c,character_A,character_B);

        break;
    case 7:
        printf("Podaj bok pola a \n");
        scanf("%d",&a);
        printf("Podaj bok szachwonicy \n");
        scanf("%d",&b);
        printf("Podaj pierwszy znak \n");
        scanf(" %c",&character_A);
        printf("Podaj drugi znak \n");
        scanf(" %c",&character_B);
        print_chess(a,b,character_A,character_B);
        break;
    }
    }while(select != 0);
}

int main(){

    
   
   
    menu();

   
    return 0;
}